# Solution overview

The Red Hat OpenShift Container Platform (OCP) on HPE SimpliVity reference configuration is a complete solution from Hewlett Packard Enterprise that includes all the hardware, software, professional services, and support you need to deploy an enterprise-ready Kubernetes platform, allowing you to get up and running quickly and efficiently. The solution takes HPE SimpliVity infrastructure and combines it with Red Hat's enterprise-grade OpenShift Container Platform, popular open source tools, along with deployment and advisory services from HPE Pointnext.

`Kubernetes` is an orchestration system for managing container-based applications. Kubernetes empowers developers to utilize new architectures like Microservices and Serverless that require developers to think about application operations in ways they may not have considered before. These software architectures can blur the lines between traditional development and application operations.

`Red Hat OpenShift Container Platform` expands enterprise Kubernetes with full-stack automated operations to manage hybrid cloud and multi-cloud deployments.

`HPE SimpliVity` is an enterprise-grade hyper-converged platform uniting best-in-class data services with the world's best-selling server.
