# Solution configuration

The Ansible playbooks create VMs according to the following specifications.

## Bootstrap node

A single bootstrap node is required to assist in the OCP deployment. You can delete the bootstrap VM after the cluster has been successfully deployed.

|VM|Number|OS|Sizing|Comments|
|:-------|:---:|:---|:----------|:----------|
|Bootstrap|1|CoreOS|4x&nbsp;vCPU<br>16GB&nbsp;RAM<br>120GB&nbsp;disk&nbsp;space|This is the RH minimum requirement|

## Supporting nodes

Two supporting nodes are deployed by default, providing DHCP and DNS services for the cluster. Two load balancer VMs and an NFS VM are also configured.

|VM|Number|OS|Sizing|Comments|
|:-------|:---:|:---|:----------|:----------|
|Support|2|RHEL&nbsp;7.6|2x&nbsp;vCPU<br>4GB&nbsp;RAM<br>60GB&nbsp;disk&nbsp;space|Providing DHCP and DNS services on the internal VLAN. You can configure one (no HA)|
|Load balancers|2|RHEL 7.6|2x&nbsp;vCPU<br>4GB RAM<br>60GB disk space|Two load balancers are deployed by default. You can configure one (no HA) or 0 where you use your own existing load balancers|
|NFS|1|RHEL 7.6|2x&nbsp;vCPU<br>4GB RAM<br>60GB disk space|Required for persistent storage for the OpenShift Registry|

## OCP cluster nodes

By default, 3 master nodes are deployed for high availability. A minimum of 2 worker nodes are required.

|VM|Number|OS|Sizing|Comments|
|:-------|:---:|:---|:----------|:----------|
|Masters|3|CoreOS|4x&nbsp;vCPU<br>16GB&nbsp;RAM<br>120GB&nbsp;disk&nbsp;space|This is the RH minimum requirement|
|Workers|2|CoreOS|2x&nbsp;vCPU<br>16GB RAM<br>120GB disk space|This is the RH minimum requirement|

Similar sizing requirements will apply for any worker nodes (CoreOS or RHEL) added to the cluster after the initial deployment.

## OCP infrastructure components

The following OpenShift Container Platform components are infrastructure components:

- Kubernetes and OpenShift Container Platform control plane services that run on masters
- The default router
- The container image registry
- The cluster metrics collection, or monitoring service
- Cluster aggregated logging
- Service brokers

Any node that runs any other container, pod, or component is a worker node that your Red Hat OpenShift subscription must cover.

The default router, image registry and monitoring service are initially deployed on the two worker nodes. These can be rescheduled onto specific infrastructure nodes as outlined in the section `Post deployment tasks`.

Cluster logging is not deployed by default. Instructions on deploying the logging stack and scheduling its services on specific cluster nodes is described in the section `Deploying cluster logging`.
