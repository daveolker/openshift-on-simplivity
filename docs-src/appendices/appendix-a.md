# Appendix A: Bill of Materials

The following BOM contains electronic license to use (E-LTU) parts. Electronic software license delivery is now available in most countries. HPE recommends purchasing electronic products over physical products (when available) for faster delivery and for the convenience of not tracking and managing confidential paper licenses. For more information, please contact your reseller or an HPE representative.

**Note:** Part numbers are at time of publication and subject to change. The bill of materials does not include complete support options or other rack and power requirements. If you have questions regarding ordering, please consult with your HPE Reseller or HPE Sales Representative for more details. [hpe.com/us/en/services/consulting.html](http://hpe.com/us/en/services/consulting.html).

**Table.** Bill of Materials

|Quantity|Part&#160;number|Description|
|:-------|:--------|:----------|
|1|868703-B21|HPE ProLiant DL380 Gen10 8SFF Configure-to-order Server
|1|826870-L21|HPE DL380 Gen10 Intel Xeon-Gold 6132 (2.6GHz/14-core/140W) FIO Processor Kit
|1|826870-B21|HPE DL380 Gen10 Intel Xeon-Gold 6132 (2.6GHz/14-core/140W) Processor Kit
|12|815100-B21|HPE 32GB (1x32GB) Dual Rank x4 DDR4-2666 CAS-19-19-19 Registered Smart Memory Kit
|1|826687-B21|HPE DL38X Gen10 2SFF Premium HDD Front NVMe or Front/Rear SAS/SATA Kit
|2|872475-B21|HPE 300GB SAS 12G Enterprise 10K SFF (2.5in) SC 3yr Wty Digitally Signed Firmware HDD
|5|P04478-B21|HPE 1.92TB SATA 6G Read Intensive SFF (2.5in) SC 3yr Wty Digitally Signed Firmware SSD
|1|870548-B21|HPE DL Gen10 x8/x16/x8 Riser Kit
|1|P01366-B21|HPE 96W Smart Storage Battery (up to 20 Devices) with 145mm Cable Kit
|1|804331-B21|HPE Smart Array P408i-a SR Gen10 (8 Internal Lanes/2GB Cache) 12G SAS Modular Controller
|1|817709-B21|HPE Ethernet 10/25Gb 2-port 631FLR-SFP28 Adapter
|1|867810-B21|HPE DL38X Gen10 High Performance Temperature Fan Kit
|2|8830272-B21|HPE 1600W Flex Slot Platinum Hot Plug Low Halogen Power Supply Kit

## Software Licenses

Licenses are required for the following software components:

- VMware
- Red Hat Enterprise Linux Server
- Red Hat OpenShift Container Platform
