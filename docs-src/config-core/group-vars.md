# Inventory group variables

## Group files

The following files in the `group_vars` folder contain variable definitions for each group of OCP cluster nodes. These group files facilitate more sophisticated settings, such as additional network interfaces.

|File|Description|
|:---|:----------|
|`group_vars/bootstrap.yml`|Variables defined for the node in the `[bootstrap]` group|
|`group_vars/infrastructure.yml`|Variables defined for all nodes in the `[infrastructure]` group|
|`group_vars/loadbalancer.yml`|Variables defined for all nodes in the `[loadbalancer]` group|
|`group_vars/nfs.yml`|Variables defined for all nodes in the `[nfs]` group|
|`group_vars/master.yml`|Variables defined for all nodes in the `[master]` group|
|`group_vars/rhcos_worker.yml`|Variables defined for all nodes in the `[rhcos_worker]` group|
|`group_vars/rhel_worker.yml`|Variables defined for all nodes in the `[rhel_worker]` group|

## Overriding group variables

If you wish to configure individual nodes with different specifications to the ones defined by the group, it is possible to declare the same variables at the individual node level in the `hosts` inventory file, overriding the values in the group-level variable file. For instance, if the default CPU and memory resource limits defined in for your worker nodes in the `group_vars/worker.yml` file are not sufficient for all worker nodes, you can override these values in their respective `hosts` entries.  For example, if these are the CPU, RAM, and disk limits specified at the group level:

```bash
cpus: '4'                                               # Number of vCPUs
ram: '16384'                                            # RAM size in MBs
disk1_size: '120'                                       # Disk size in GBs
```

you can override these values in the individual node entries in the `hosts` file:

```bash
[rhcos_worker]
ocp-worker0   ansible_host=10.15.155.213
ocp-worker1   ansible_host=10.15.155.214
ocp-worker2   ansible_host=10.15.155.215  cpus=8 ram=32768  # Larger worker node for EFK
ocp-worker3   ansible_host=10.15.155.216  cpus=8 ram=32768  # Larger worker node for EFK
ocp-worker4   ansible_host=10.15.155.217  cpus=8 ram=32768  # Larger worker node for EFK
```

In the example above, three CoreOS worker nodes (ocp-worker2, ocp-worker3, ocp-worker4) are allocated 2 times more virtual CPU cores and double the RAM compared to the rest of the CoreOS worker nodes.

## Common variables across all groups

The following variables apply to all node groups:

|Variable|Scope|Description|
|:-------|:----|:----------|
|`ip_addr`|All nodes|IP address in CIDR format to be given to a node|
|`esxi_host`|All nodes|ESXi host where the node will be deployed. If the cluster is configured with DRS, this option will be overridden|
|`cpus`|All nodes/groups|Number of virtual CPU cores to assign to a VM or a group of VMs|
|`ram`|All nodes/groups|Amount of RAM in MB to assign to a VM or a group of VMs|
|`disk1_size`|All nodes/groups|Size of the  disk in GB to attach to a VM or a group of VMs|
|`folder`|All nodes/groups|The vCenter folder where the VM will be stored|
|`template`|All nodes/groups|Override the default template for the node or group. <br><br>The Bootstrap node (`[bootstrap]` uses the CoreOS template specified by `<<master_template>>`.<br><br>Support nodes (`[support]`, `[loadbalancer]`, `[nfs]`) use the RHEL template specified by `<<support_template>>` by default. <br><br>The master nodes (`[master]`) use the CoreOS template specified by `<<master_template>>`. <br><br>The CoreOS worker nodes (`[rhcos_worker]`) use the CoreOS template specified by `<<worker_template>>`, which is typically the same as `<<master_template>>`.<br><br>For RHEL worker nodes (`[rhel_worker]`), you should set the group default to name of the RHEL template you typically use.|
|`ova_path`|All nodes/groups|Name of the OVA used to import the template. <br><br>The Bootstrap node (`[bootstrap]` uses the CoreOS OVA specified by `<<master_ova_path>>`.<br><br>Infrastructure and supporting nodes (`[infrastructure]`, `[loadbalancer]`, `[nfs]`) use the RHEL OVA specified by `<<infra_ova_path>>` by default. <br><br>The master nodes (`[master]`) use the CoreOS OVA specified by `<<master_ova_path>>`. <br><br>The CoreOS worker nodes (`[rhcos_worker]`) use  the CoreOS OVA specified by `<<worker_ova_path>>`, which is typically the same as `<<master_template>>`.<br><br>For RHEL worker nodes (`[rhel_worker]`), you should set the group default to name of the RHEL OVA you typically use.|

## Variables specific to CoreOS nodes

|Variable|Scope|Description|
|:-------|:----|:----------|
|`ignition_file`|`[bootstrap]`<br>`[master]`<br>`[worker]`|Location of the ignition data. The default directory is defined by ``<<install_dir>>`` and the file name depends on the group: <br><br>`append-bootstrap.ign` for `[bootstrap]` node<br><br>`master.ign` for `[master]` nodes<br><br>`worker.ign` for `[worker]` nodes|
|`initial_port_check`|`[bootstrap]`<br>`[master]`<br>`[worker]`|Port to use when testing connectivity to the node. <br><br>For `[bootstrap]` node, port `22` <br><br>For `[master]` nodes, port `6443`<br><br>For `[worker]` nodes, port `22`|
|`initial_port_check_timeout`|`[bootstrap]`<br>`[master]`<br>`[worker]`|Delay in seconds before testing for connectivity. <br><br>For `[bootstrap]` node, wait `10` seconds after initial power on for the port to be open.<br><br>For `[master]` nodes, wait up to `900` seconds for the OpenShift API to be available<br><br>For `[worker]` nodes, wait `10` seconds.|

## Disk specifications

```bash
disks_specs:
  - size_gb:  '{{ disk1_size }}'
    type: thin
    datastore: "{{ datastores | random }}"
```

## Custom values for CoreOS nodes

```bash
customvalues:
  - key: disk.EnableUUID
    value: "TRUE"
  - key: guestinfo.ignition.config.data
    value: "{{ ignition_data }}"
  - key: guestinfo.ignition.config.data.encoding
    value: base64
  - key: sched.cpu.latencySensitivity
    value: High
```

The `ignition_data` variable is generated from the ignition file in the playbook `playbooks/roles/vspherevm/tasks/provision_vm.yml`

```bash
- name: Load Ignition Data
  set_fact:
    ignition_data: "{{ lookup('file', ignition_file) | b64encode }}"
```

## Group variables for infrastructure and support nodes

The nodes in the groups `[infrastructure]`,`[loadbalancer]`, and `[nfs]` are typically RHEL nodes and have the following common definitions.

### customvalues

```bash
customvalues:
  - key: guestinfo.metadata
    value: "{{ cloud_init_metadata_data }}"
  - key: guestinfo.metadata.encoding
    value: base64
  - key: guestinfo.userdata
    value:  "{{ cloud_init_users_data }}"
  - key: guestinfo.userdata.encoding
    value: base64
```

### networks

```bash
networks:
  - name: '{{ vm_portgroup }}'
```

Load balancers have extra entry in `networks`:

```bash
networks:
  - name: '{{ vm_portgroup }}'
  - name: '{{ frontend_vm_portgroup }}'
```

### cloud_init_networks

```bash
cloud_init_networks:
  version: 1
  config:
  - type: physical
    name: ens192
    subnets:
      - type: static
        address: "{{ hostvars[inventory_hostname].ansible_host }}"
        netmask: "{{ dhcp_subnet | ipaddr('netmask') }}"
        gateway: "{{ gateway }}"
        dns_nameservers: "{{ internal_dns_servers | union(dns_servers) }}"
        dns_search:
          - "{{ cluster_name }}.{{ domain_name }}"
```

Load balancer nodes have extra configuration settings for the second (frontend) network

```bash
cloud_init_networks:
  version: 1
  config:
  - type: physical
    name: ens192
...
  - type: physical
    name: ens224
    subnets:
      - type: static
        address: "{{ hostvars[inventory_hostname].frontend_ipaddr | ipaddr('address') }}"
        netmask: "{{ hostvars[inventory_hostname].frontend_ipaddr | ipaddr('netmask') }}"
        gateway: "{{ frontend_gateway }}"
```
