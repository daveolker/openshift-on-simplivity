# About this release

This release has been tested using Red Hat OpenShift Container Platform (OCP) 4.1.16, which is based on Kubernetes 1.13.

OpenShift Container Platform 4.1 supports Red Hat Enterprise Linux (RHEL) 7.6, as well as Red Hat Enterprise Linux CoreOS (RHCOS) 4.1. You must use CoreOS for the master control plane nodes and either CoreOS or RHEL 7.6 for all worker/compute nodes. Because version 7.6 is the only non-RHCOS version of Red Hat Enterprise Linux supported on compute nodes, you must not upgrade any RHEL 7.6 compute nodes to version RHEL 8.

You must install the OpenShift Container Platform cluster on a VMware vSphere instance running version 6.5 or 6.7 U2 or later. VMware recommends using vSphere Version 6.7 U2 or later with your OpenShift Container Platform cluster.

See the Red Hat OCP 4.1 release notes at [https://docs.openshift.com/container-platform/4.1/release_notes/ocp-4-1-release-notes.html](https://docs.openshift.com/container-platform/4.1/release_notes/ocp-4-1-release-notes.html)

OpenShift Container Platform 4.1 requires all machines, including the computer that you run the installation process on, to have direct Internet access to pull images for platform containers and provide a limited amount of telemetry data to Red Hat. OCP 4.1 does not support the use of a proxy server to reach the Internet. By installing OpenShift Container Platform 4, you accept the terms of Red Hat's data collection policy. Learn more about the data collected at
[https://docs.openshift.com/container-platform/4.1/telemetry/about-telemetry.html](https://docs.openshift.com/container-platform/4.1/telemetry/about-telemetry.html)