# Introduction

The following section covers some common post-deployment and validation tasks, including:

- Logging into the OCP cluster for the first time
- Deploying a sample application
- Configuring external routes
- Exposing the image registry
