# Deploying a sample application

Create a new project called "my-nginx-example":

```bash
$ oc new-project my-nginx-example

Now using project "my-nginx-example" on server "https://api.ocp.hpecloud.org:6443".

You can add applications to this project with the 'new-app' command. For example, try:

    oc new-app django-psql-example

to build a new example application in Python. Or use kubectl to deploy a simple Kubernetes application:

    kubectl create deployment hello-node --image=gcr.io/hello-minikube-zero-install/hello-node
```

Deploy a new application:

```bash
$ oc new-app --template=openshift/nginx-example --name=my-nginx-example --param=NAME=my-nginx-example

--> Deploying template "openshift/nginx-example" for "openshift/nginx-example" to project my-nginx-example

     Nginx HTTP server and a reverse proxy
     ---------
     An example Nginx HTTP server and a reverse proxy (nginx) application that serves static content. For more information about using this template, including OpenShift considerations, see https://github.com/sclorg/nginx-ex/blob/master/README.md.

     The following service(s) have been created in your project: my-nginx-example.

     For more information about using this template, including OpenShift considerations, see https://github.com/sclorg/nginx-ex/blob/master/README.md.

     * With parameters:
        * Name=my-nginx-example
        * Namespace=openshift
        * NGINX Version=1.12
        * Memory Limit=512Mi
        * Git Repository URL=https://github.com/sclorg/nginx-ex.git
        * Git Reference=
        * Context Directory=
        * Application Hostname=
        * GitHub Webhook Secret=n2RnraoYqYKWwWSOxmHXodDThyL8qOwLXqRyYWbU # generated
        * Generic Webhook Secret=nLGGtYKDupWHq4yCqUg5EiWRtVKV4e3lxEEsfDup # generated

--> Creating resources ...
    service "my-nginx-example" created
    route.route.openshift.io "my-nginx-example" created
    imagestream.image.openshift.io "my-nginx-example" created
    buildconfig.build.openshift.io "my-nginx-example" created
    deploymentconfig.apps.openshift.io "my-nginx-example" created
--> Success
    Access your application via route 'my-nginx-example-my-nginx-example.apps.ocp.hpecloud.org'
    Build scheduled, use 'oc logs -f bc/my-nginx-example' to track its progress.
    Run 'oc status' to view your app.
```

The output shows that a service and route have been created for the application, and tells you to use the `oc status` command to check your application:

```bash
$ oc status
In project my-nginx-example on server https://api.ocp.hpecloud.org:6443

http://my-nginx-example-my-nginx-example.apps.ocp.hpecloud.org (svc/my-nginx-example)
  dc/my-nginx-example deploys istag/my-nginx-example:latest <-
    bc/my-nginx-example source builds https://github.com/sclorg/nginx-ex.git on openshift/nginx:1.12
    deployment #1 deployed 23 minutes ago - 1 pod
```

The details of the service can be obtained using the `oc get svc` command:

```bash
$ oc get svc my-nginx-example
NAME               TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
my-nginx-example   ClusterIP   172.30.155.90   <none>        8080/TCP   3m26s
```

The route created for the example application  uses the application name `my-nginx-example`, the cluster name `ocp`, and the domain name `hpecloud.org`.

```bash
$ oc get route my-nginx-example

NAME               HOST/PORT                                                 PATH   SERVICES           PORT
my-nginx-example   my-nginx-example-my-nginx-example.apps.ocp.hpecloud.org          my-nginx-example   <all>
```

Use the route in your browser to access the application:

!["Nginx example - backend network"][media-nginx-example-backend-png]

**Figure: Nginx example - backend network**

[media-nginx-example-backend-png]:<../images/nginx-example-backend.png> "Figure: Nginx example - backend network"
