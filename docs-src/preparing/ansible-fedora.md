# Create the Ansible node on Fedora

The Ansible controller must be deployed using a Fedora-based system in order to take advantage of the built-in support for Python 3. HPE recommends using Fedora 29 or later and running Ansible 2.8.1 or later.  Also, the Ansible controller node must be connected to a network with proxy-free connectivity to the Internet.

The playbooks should be run from a non-privileged account. HPE recommends using an account named `core` as this will ensure consistency with deployed CoreOS machines that have a built-in account `core`. In addition, any non-CoreOS VMs created by the playbooks will have a user account created with the same name as the user on the Ansible controller node that ran the playbooks.

## Create the Fedora VM

Create a Virtual Machine with the following characteristics:

- **Guest OS:** Red Hat Fedora Server 29 (64-bit)
- **Disk:** 50G (thin provisioning)
- **CPU:** 2
- **RAM:** 4 GB
- **Ethernet Adapter:** VMXNET3, connected to your Ansible or management network

Install Fedora Server 29 using the x64 64-bit ISO image. In the `Software Selection` section, choose:

- **Base Environment:** Fedora Server Edition
- **Add-Ons for Selected Environment:** Guest Agents

Select your language, keyboard layout, and time zone settings and re-boot when the installation finishes. Configure your networking and check your connectivity before moving on to the next section.

## Password-less sudo

Configure password-less `sudo` for the `core` user on the Ansible controller node by logging in as `root` and issuing the command:

```bash
$ usermod -a -G wheel core
```

The default `/etc/sudoers` file contains two lines for the group `wheel`. As the root user, run `visudo` to uncomment the %wheel line containing the string `NOPASSWD:` and comment out the other %wheel line. When you are done, it should look like this:

```bash
## Allows people in group wheel to run all commands
#%wheel ALL=(ALL)       ALL

## Same thing without a password
%wheel ALL=(ALL)       NOPASSWD: ALL
```

The advantage of using `visudo` is that it will validate the changes to the file.

## Install Ansible and required dependencies

Log into the Ansible controller node as the `core` user and run the following commands to install `git`, `ansible` and the required Python dependencies:

```bash
$ sudo dnf update -y
$ sudo dnf install -y git ansible python3-netaddr python3-requests python3-pyvmomi python3-pip

$ cd /usr/bin
$ ln -s python3.7 python
```
