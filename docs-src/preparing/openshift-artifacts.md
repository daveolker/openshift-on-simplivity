# OpenShift artifacts

## Documentation

### Architecture

An introduction to the OpenShift Container Platform architecture is available at:
[https://docs.openshift.com/container-platform/4.1/architecture/architecture.html](https://docs.openshift.com/container-platform/4.1/architecture/architecture.html).

### Installation

OpenShift Container Platform 4.1 supports two types of installations:

- **Installer-Provisioned Infrastructure** (IPI) where the installation program is responsible for deploying and maintaining the underlying infrastructure, as well as the cluster itself. An example of this is the deployment to Amazon Web Services (AWS).
- **User-Provisioned Infrastructure** (UPI) where the user is responsible for setting up and maintaining the underlying infrastructure, and the installation program deploys the cluster on top of this infrastructure. An example of this style of installation is deployment to bare metal or to VMware vSphere.

As this solution runs on HPE SimpliVity, it follows the methodology for User-Provisioned Infrastructure, and it helps you to deploy the underlying VMs for the cluster itself and the supporting nodes.

More information about the general installation process is available at:
[https://docs.openshift.com/container-platform/4.1/architecture/architecture-installation.html](https://docs.openshift.com/container-platform/4.1/architecture/architecture-installation.html).

Documentation specific to installing OCP 4.1 on a VMware cluster is available at:
[https://docs.openshift.com/container-platform/4.1/installing/installing_vsphere/installing-vsphere.html](https://docs.openshift.com/container-platform/4.1/installing/installing_vsphere/installing-vsphere.html).

### Release notes

OpenShift Container Platform provides almost-weekly bug fix updates, so it is important to follow the release details provided and to upgrade the OCP software regularly. The OpenShift release notes for version 4.1 are available at:
[https://docs.openshift.com/container-platform/4.1/release_notes/ocp-4-1-release-notes.html](https://docs.openshift.com/container-platform/4.1/release_notes/ocp-4-1-release-notes.html).

At the time of writing, the latest version of OpenShift is 4.1.16 and this is reflected in the sample download instructions below. If you want to use a newer version of the OCP software, you must modify the URLs to match the desired version.

## Red Hat CoreOS

Red Hat CoreOS is used for the master nodes in the OCP cluster as well as the bootstrap node used to initially bring-up the cluster, as well as any CoreOS-based worker/compute nodes.

Download the Red Hat CoreOS OVA for OCP 4.1 from: [https://mirror.openshift.com/pub/openshift-v4/dependencies/rhcos/4.1/latest/rhcos-4.1.0-x86_64-vmware.ova](https://mirror.openshift.com/pub/openshift-v4/dependencies/rhcos/4.1/latest/rhcos-4.1.0-x86_64-vmware.ova).

To use this OVA file for the master nodes in the cluster, set the variable `master_ova_path` in the `group_vars\all\vars.yml` file to the path where the CoreOS OVA file is located on your Ansible controller node. The OVA used by worker nodes is determined by the `worker_ova_path` variable and will default to the same OVA as that used for master nodes.

As part of the installation process, templates will be created in vCenter from the master and worker OVA files. These templates will be named based on the variables `master_template` and `worker_template` in `group_vars\all\vars.yml`.

## Clients

The latest version of the OCP client software is available at: [https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest](https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest).

The 4.1.16 version of the `openshift-install` software used by the playbooks to install the cluster can be downloaded from: [https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-install-linux-4.1.16.tar.gz](https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-install-linux-4.1.16.tar.gz).

Download and unpack the file. Set the variable `ocp_installer_path` in the `group_vars\all\vars.yml` file to match the path where the `openshift-install` program is located on your Ansible controller node.

Two additional programs are used to interact with OCP once the cluster is fully deployed: `oc` and `kubectl`. These programs are available in a single tar file at: [https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-client-linux-4.1.16.tar.gz](https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-client-linux-4.1.16.tar.gz).

Download and unpack the file. Set the variable `ocp_oc_path` in the `group_vars\all\vars.yml` file to match the path where the `oc` program is located on your Ansible controller node. Set the variable `ocp_kubectl_path` to match the path where the `kubectl` program resides on your Ansible controller node.

### Downloading and unpacking

An example of the commands used to download and unpack the required software is shown below:

```bash
$ mkdir ~/kits
$ cd ~/kits
$ wget https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-client-linux-4.1.16.tar.gz
$ wget https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-install-linux-4.1.16.tar.gz
$ wget https://mirror.openshift.com/pub/openshift-v4/dependencies/rhcos/4.1/latest/rhcos-4.1.0-x86_64-vmware.ova
$ tar -xvf openshift-client-linux-4.1.16.tar.gz
$ tar -xvf openshift-install-linux-4.1.16.tar.gz
```

## Pull secret

From the OpenShift Infrastructure Providers page, download your personalized installation pull secret. This pull secret allows you to authenticate with the services that are provided by the included authorities, including Quay.io, which serves the container images for OpenShift Container Platform components. Copy this pull secret to the variable `pull_secret` in your Ansible vault file `group_vars\all\vault.yml`.
