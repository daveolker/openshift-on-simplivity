# Verify prerequisites

Before you start deployment, you must assemble the information required to assign values for each variable used by the playbooks. The variables are fully documented in the section `Configuring the solution`. A brief overview of the information required is presented in the table below.

|Component|Details|Comments|
|:-------|:---|:----------|
|Virtual Infrastructure|ESXi cluster of three machines<br><br>vCenter 6.7U1|The FQDN of your vCenter server and the name of the Datacenter. You will also need administrator credentials in order to create templates and deploy virtual machines.|
|L3 Network requirements|1x&nbsp;Bootstrap<br><br>2x&nbsp;Support,&nbsp;2x&nbsp;Load&nbsp;balancer,&nbsp;1x&nbsp;NFS<br><br>3x master,&nbsp;2x worker|You will need one IP address for each VM configured in the Ansible inventory. <br>You will also need to allocate at least 2 additional addresses for the virtual IPs used by the load balancers.|
|NTP Services|IP addresses of your time servers (NTP)|You need time services configured in your environment. The deployed solution uses certificates that are time-sensitive.|
|Red Hat Network Subscription|Organization ID and authorization key<br>Alternatively a username and password.|The subscription must contain valid licenses for both Red Hat Enterprise Linux 7.6 and OpenShift Container Platform 4|
