# Hardware

## HPE SimpliVity

The Operations environment is comprised of three HPE SimpliVity 380 Gen10 servers. HPE recommends dual socket HPE SimpliVity systems with at least 14 CPU cores per socket (28 total cores per system) for optimal performance and support during HA failover scenarios. Since the HPE SimpliVity technology relies on VMware virtualization, the servers are managed using vCenter.

For more details, see the Bill of Materials in Appendix A.
