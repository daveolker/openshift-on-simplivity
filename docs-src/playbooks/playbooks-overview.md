# Playbook summary

## Initial deployment

The Ansible playbook used to initially deploy the OpenShift Container Platform on HPE SimpliVity solution is:

- `site.yml`

## Post deployment playbooks

A number of playbooks are provided to help with post-deployment tasks:

- `playbooks/scale.yml` used to add RHCOS or RHEL 7.6 worker nodes
- `playbooks/efk.yml` used to deploy the Elasticsearch, Fluentd and Kibana (EFK) logging stack
- `backup_etcd.yml` used in the OCP cluster backup and restore procedure
- `playbooks/ldap.yml` used for LDAP integration
- `playbooks/clean.yml` a convenience playbook for stripping down a cluster, potentially before redeploying
