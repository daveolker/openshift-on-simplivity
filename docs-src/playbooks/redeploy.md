# Redeployment

The playbook `playbooks/clean.yml` is a convenience playbook for stripping down a cluster. This can be very useful in proof-of-concept environments, where you want to regularly tear down and re-deploy your test cluster with different settings.

Run the playbook as follows:

```bash
$ cd ~/OpenShift-on-SimpliVity
$ ansible-playbook -i hosts playbooks/clean.yml --vault-password-file .vault_pass
```

The playbook will un-register any Red Hat systems from the Red Hat Network and then delete all VMs and templates.
