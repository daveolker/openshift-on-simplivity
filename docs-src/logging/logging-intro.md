# Introduction to cluster logging

You can deploy cluster logging to aggregate logs for a range of OpenShift Container Platform services.

The built-in cluster logging components are based upon Elasticsearch, Fluentd, and Kibana (EFK). The collector, Fluentd, is deployed to each node in the OpenShift Container Platform cluster. It collects all node and container logs and writes them to Elasticsearch (ES). Kibana is the centralized, web UI where users and administrators can create rich visualizations and dashboards with the aggregated data.

## About cluster logging components

There are four different types of cluster logging components:

- `logStore:` This is where the logs will be stored. The current implementation is `Elasticsearch`.

- `collection:` This is the component that collects logs from the nodes, formats them, and stores them in the logStore. The current implementation is `Fluentd`.

- `visualization:` This is the UI component used to view logs, graphs, charts, and so forth. The current implementation is `Kibana`.

- `curation:` This is the component that trims logs by age. The current implementation is `Curator`.

For more information about the OpenShift Cluster Logging facility, see the documentation at [https://docs.openshift.com/container-platform/4.1/logging/efk-logging.html](https://docs.openshift.com/container-platform/4.1/logging/efk-logging.html).
